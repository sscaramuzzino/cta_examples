**CLINICAL TECHNOLOGY EXAMPLES**

This repository contains examples and data for clinical technology assessment course @ polimi (2017)

---

## Files and meaning



1. SDO_B19073_SET_DIPX_S0003_AN.xml		;	file anagrafiche pazienti
2. SDO_B19073_SET_DIPX_S0003_CL.xml		;	file prestazioni erogate a pazienti
3. sdo_handling.ipynb					;	notebook ipython per il calcolo delle prestazioni com maggiore difficoltà di erogazione
